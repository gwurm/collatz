#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

def cycle_length (n) :
        assert n > 0
        c = 1
        while n > 1 :
            if (n % 2) == 0 :
                n = (n // 2)
            else :
                n = (3 * n) + 1
            c += 1
        assert c > 0
        return c
        
def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    
    cache = {}
    
    for x in range (1,3000):
        temp = cycle_length(x)
        cache[x] = temp
        #print(temp)
        
    #print(cache)
    #input("pause")

    #max cycle length holder starting at value 1
    max = 1
    
    #check for corner case
    if i > j:
        i,j = j,i
    
    for n in range(i,j+1):
        if n in cache:
            cl = cache[n]
        else:
            cl = cycle_length(n)
            cache[n] = cl
        if cl > max:
            max = cl  
    #print("")
    #print("")
    #print("")
    #print("")
    #print(cache)
    return max
    
   
# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
